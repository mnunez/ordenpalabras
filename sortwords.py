#!/usr/bin/env pythoh3

# MARTA NÚÑEZ GARCÍA

'''
Program to order a list of words given as arguments
'''

import sys

# Función "is_lower". En primer lugar se comprueba la longitud de las palabras que posteriormente se comparan
# alfabéticamente, ya que de esta manera, el rango del bucle "FOR" implementado posteriormente que itera en cada
# una de las palabras, es distinto según dicha longitud. Se compara cada una de las palabras letra a letra
# (transformándolo en minúsculas). Si son distintas letras, se comprueba la que es anterior alfabéticamente (menor).
# En este caso la función devuelve "True". En caso contrario, devuelve "False".
def is_lower(first: str, second: str):
    """Return True if first is lower (alphabetically) than second

    Order is checked after lowercasing the letters
    `<` is only used on single characteres.
    """
    if len(first) < len(second):
        leng = len(first)
    else:
        leng = len(second)
    for n in range(leng):
        if first[n].lower() != second[n].lower():
            if first[n].lower() < second[n].lower():
                return True
            else:
                return False


# Función "get_lower". Se realiza un bucle "FOR" que itera en un rango entre la posición pivote introducida y la
# longitud de la lista proporcionada con el fin de comprobar mediante la función realizada "is_lower" cada una de
# las palabras de la lista (desde la posición pivote hacia la derecha, incluyendo dicha posición). Si la función
# "is_lower" proporciona "False", la posición de la palabra menor alfabéticamente será la de dicha posición. La
# función devuelve la posición de la palabra menor alfabéticamente.
def get_lower(words: list, pos: int) -> int:
    """Get lower word, for words right of pos (including pos)"""
    for i in range(pos, len(words)):
        if is_lower(words[pos], words[i]) == False:
            pos = i
    return pos


# Función "sort". Se crea en primer lugar una lista vacía. Posteriormente, se realiza un bucle "FOR" que itera en
# un rango desde 0 hasta la longitud de la lista proporcionada. Se aplica la función realizada "get_lower" con el fin
# de obtener las posiciones desde la palabra menor alfabéticamente hasta la mayor (de la lista). Se añaden a la lista
# vacía las palabras según las posiciones obtenidas.
def sort(words: list) -> list:
    """Return the list of words, ordered alphabetically"""
    lista = []
    for m in range(0, len(words)):
        lower = get_lower(words,m)
        lista.append(words[lower])
        words[lower] = words[m]
    return lista


# Función "show". Se obtienen strings a partir de la lista proporcionada. Posteriormente, la función devuelve la
# impresión por pantalla de las palabras ordenadas alfabéticamente.
def show(words: list):
    """Show words on screen, using print()"""
    write = " ".join(words)
    imp = print(write)
    return imp


def main():
    words: list = sys.argv[1:]
    ordered: list = sort(words)
    show(ordered)


if __name__ == '__main__':
    main()
